<?php

/* For handling general settings for node and blocks such as zoom-level, type of map and more */
function gmap_general_config($form, &$form_state) {
	$form = array();

	$form['nodes'] = array(
		'#type' => 'fieldset',
		'#title' => t('Nodes'),
		'#description' => 'Settings for map on Nodes',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE, 
	);

	$form['nodes']['nodes-zoom'] = array(
		'#type' => 'textfield',
		'#title' => 'Zoom',
		'#description' => 'Default zoom-level for map',
		'#default_value' => '5',
		'#size' => '3',
		'#required' => TRUE,
	);

	$form['nodes']['nodes-animation'] = array(
		'#type' => 'select',
		'#title' => 'Marker Animation',
		'#options' => array('google.maps.Animation.BOUNCE' => 'Bounce', 'google.maps.Animation.DROP' => 'Drop'),
		'#default_value' => 'google.maps.Animation.BOUNCE',
	);

	$adetails = "<pre>
<strong>BOUNCE</strong> Marker bounces until animation is stopped.
<strong>DROP</strong> Marker falls from the top of the map ending with a small bounce
</pre>";

	$form['nodes']['adetails'] = array(
		'#type' => 'markup',
		'#markup' => $adetails,
	);

	$form['nodes']['marker-draggable'] = array(
		'#type' => 'select',
		'#title' => 'Marker Draggable',
		'#options' => array('true' => 'True', 'false' => 'False'),
		'#default_value' => 'false',
		'#description' => 'Allow marker to be Draggable',
	);

	$types = array();
	$types['google.maps.MapTypeId.ROADMAP'] = 'MapTypeId.ROADMAP';
	$types['google.maps.MapTypeId.SATELLITE'] = 'MapTypeId.SATELLITE';
	$types['google.maps.MapTypeId.HYBRID'] = 'MapTypeId.HYBRID';
	$types['google.maps.MapTypeId.TERRAIN'] = 'MapTypeId.TERRAIN';

	$form['nodes']['nodes-types'] = array(
		'#type' => 'select',
		'#title' => 'Map Types',
		'#options' => $types,
		'#default_value' => 'MapTypeId.ROADMAP',
	);

	$details = "<pre>
<strong>ROADMAP</strong> displays the default road map view. This is the default map type.
<strong>SATELLITE</strong> displays Google Earth satellite images
<strong>HYBRID</strong> displays a mixture of normal and satellite views
<strong>TERRAIN</strong> displays a physical map based on terrain information.
</pre>";
	$form['nodes']['details'] = array(
		'#type' => 'markup',
		'#markup' => $details,
	);
	
	$ioptions = array('default' => 'Default','circle' => 'Circle', 'image' => 'Image', 'star' => 'Star');

	$form['nodes']['nodes-icon-type'] = array(
		'#type' => 'select',
		'#title' => 'Icon Types',
		'#options' => $ioptions,
	);

	$form['nodes']['categories'] = array(
		'#type' => 'fieldset',
		'#title' => 'Categories',
		'#collapsible' => TRUE,
		'#collapsed' => FALSE,		
		);
	
	$form['nodes']['categories']['#states'] = array(
		'visible' => array(
			':input[name=nodes-icon-type]' => array('value' => 'image'),
	),);

	$sports = array(
		'archery.png' => 'placeHolder',
		'atv.png' => 'placeHolder',
		'australianfootball.png' => 'placeHolder',
		'avalanche1.png' => 'placeHolder',
		'badminton-2.png' => 'placeHolder',
		'baseball.png' => 'placeHolder',
		'basketball.png' => 'placeHolder',
		'beachvolleyball.png' => 'placeHolder',
		'bike_downhill.png' => 'placeHolder',
		'bike_rising.png' => 'placeHolder',
		'billiard-2.png' => 'placeHolder',
		'boardercross.png' => 'placeHolder',
		'bobsleigh.png' => 'placeHolder',
		'bollie.png' => 'placeHolder',
		'boxing.png' => 'placeHolder',
		'chronometer.png' => 'placeHolder',
		'climbing.png' => 'placeHolder',
		'coldfoodcheckpoint.png' => 'placeHolder',
		'cricket.png' => 'placeHolder',
		'cup.png' => 'placeHolder',
		'curling-2.png' => 'placeHolder',
		'cycling.png' => 'placeHolder',
		'cycling_feed.png' => 'placeHolder',
		'cycling_sprint.png' => 'placeHolder',
		'deepseafishing.png' => 'placeHolder',
		'discgolf.png' => 'placeHolder',
		'diving.png' => 'placeHolder',
		'finish.png' => 'placeHolder',
		'fishing.png' => 'placeHolder',
		'fitness.png' => 'placeHolder',
		'fourbyfour.png' => 'placeHolder',
		'ft.png' => 'placeHolder',
		'glasswater.png' => 'placeHolder',
		'golfing.png' => 'placeHolder',
		'handball.png' => 'placeHolder',
		'hanggliding.png' => 'placeHolder',
		'hiking.png' => 'placeHolder',
		'horseriding.png' => 'placeHolder',
		'hotfoodcheckpoint.png' => 'placeHolder',
		'hunting.png' => 'placeHolder',
		'icehockey.png' => 'placeHolder',
		'iceskating.png' => 'placeHolder',
		'indoor-arena.png' => 'placeHolder',
		'jogging.png' => 'placeHolder',
		'judo.png' => 'placeHolder',
		'karate.png' => 'placeHolder',
		'karting.png' => 'placeHolder',
		'kayaking.png' => 'placeHolder',
		'kitesurfing.png' => 'placeHolder',
		'lockerroom.png' => 'placeHolder',
		);

	$form['nodes']['categories']['category-1'] = array(
		'#type' => 'fieldset',
		'#title' => 'Sports',
		'#collapsible' => TRUE,
		'#collapsed' => TRUE,
	);

	$form['nodes']['categories']['category-1']['nodes-icon'] = array(
		'#type' => 'radios',
		'#options' => $sports,
		'#default_value' => 'archery.png',
	);

	// $restaurants = array(
	// 	'bar.png' => 'placeHolder',
	// 	'bar_coktail.png' => 'placeHolder',
	// 	'bar_juice.png' => 'placeHolder',
	// 	'barbecue.png' => 'placeHolder',
	// 	'bed_breakfast1-2.png' => 'placeHolder',
	// 	'beergarden.png' => 'placeHolder',
	// 	'cafetaria.png' => 'placeHolder',
	// 	'coffee.png' => 'placeHolder',
	// 	'cruiseship.png' => 'placeHolder',
	// 	'fastfood.png' => 'placeHolder',
		
	// 	'fishchips.png' => 'placeHolder',
	// 	'fooddeliveryservice.png' => 'placeHolder',
	// 	'foodtruck.png' => 'placeHolder',
	// 	'gay-female.png' => 'placeHolder',
	// 	'gay-male.png' => 'placeHolder',
	// 	'gluten_free.png' => 'placeHolder',
	// 	'gourmet_0star.png' => 'placeHolder',
	// 	'hostel_0star.png' => 'placeHolder',
	// 	'hotel_0star.png' => 'placeHolder',
	// 	'icecream.png' => 'placeHolder',
		
	// 	'japanese-food.png' => 'placeHolder',
	// 	'japanese-sake.png' => 'placeHolder',
	// 	'kebab.png' => 'placeHolder',
	// 	'lodging_0star.png' => 'placeHolder',
	// 	'lodging_horseriding.png' => 'placeHolder',
	// 	'motel-2.png' => 'placeHolder',
	// 	'pizzaria.png' => 'placeHolder',
	// 	'resort.png' => 'placeHolder',
	// 	'restaurant.png' => 'placeHolder',
	// 	'restaurant_african.png' => 'placeHolder',
		
	// 	'restaurant_breakfast.png' => 'placeHolder',
	// 	'restaurant_buffet.png' => 'placeHolder',
	// 	'restaurant_chinese.png' => 'placeHolder',
	// 	'restaurant_fish.png' => 'placeHolder',
	// 	'restaurant_greek.png' => 'placeHolder',
	// 	'restaurant_indian.png' => 'placeHolder',
	// 	'restaurant_italian.png' => 'placeHolder',
	// 	'restaurant_korean.png' => 'placeHolder',
	// 	'restaurant_mediterranean.png' => 'placeHolder',
	// 	'restaurant_mexican.png' => 'placeHolder',

	// 	'restaurant_romantic.png' => 'placeHolder',
	// 	'restaurant_steakhouse.png' => 'placeHolder',
	// 	'restaurant_tapas.png' => 'placeHolder',
	// 	'restaurant_thai.png' => 'placeHolder',
	// 	'restaurant_turkish.png' => 'placeHolder',
	// 	'restaurant_vegetarian.png' => 'placeHolder',
	// 	'sandwich-2.png' => 'placeHolder',
	// 	'tajine-2.png' => 'placeHolder',
	// 	'takeaway.png' => 'placeHolder',
	// 	'teahouse.png' => 'placeHolder',
	// 	);


	// $form['nodes']['categories']['category-2'] = array(
	// 	'#type' => 'fieldset',
	// 	'#title' => 'Restaurants & Hotels',
	// 	'#collapsible' => TRUE,
	// 	'#collapsed' => TRUE,
	// );

	// $form['nodes']['categories']['category-2']['nodes-icon'] = array(
	// 	'#type' => 'radios',
	// 	// '#title' => 'Choose Image',
	// 	'#options' => $restaurants,
	// 	'#name' => 'same',
	// );
	
	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Changes',
		);
	
	$form['reset'] = array(
		'#type' => 'markup',
		'#markup' => '<input type=reset  class=form-submit />'
		);

	$general_config = variable_get('gmap_node_config','NULL');
	if($general_config != 'NULL') {
		$single = explode("#",$general_config['0']);

		$form['nodes']['nodes-zoom']['#default_value'] = $single[0];
		$form['nodes']['nodes-types']['#default_value'] = $single[1];
		$form['nodes']['nodes-animation']['#default_value'] = $single[2];
		$form['nodes']['marker-draggable']['#default_value'] = $single[3];
		$form['nodes']['nodes-icon-type']['#default_value'] = $single[4];
		$form['nodes']['categories']['category-1']['nodes-icon']['#default_value'] = $single[5];

	}		

	return $form;
}

/* Saving the required value into VARIABLE table*/
function gmap_general_config_submit($form, &$form_state) {
	// dpm($form_state['values']);

	$general_config = array();
	$nodes_zoom = $form_state['values']['nodes-zoom'];
	$nodes_mtype = $form_state['values']['nodes-types'];
	$nodes_animation = $form_state['values']['nodes-animation'];
	$marker_draggable = $form_state['values']['marker-draggable'];
	$nodes_icon_type = $form_state['values']['nodes-icon-type'];
	$nodes_image = $form_state['values']['nodes-icon'];

	array_push($general_config, $nodes_zoom . '#' . $nodes_mtype . '#' . $nodes_animation . '#' . $marker_draggable . '#' . $nodes_icon_type . '#' . $nodes_image);
	
	variable_set('gmap_node_config',$general_config);
	drupal_flush_all_caches();
	drupal_set_message("General Settings Saved. Clear Cache in case changes are not in effect");
}


/* Tutorial */
function gmap_node_tutorial($form, &$form_state) {

	$mod_path = drupal_get_path('module', 'gmap_node');
	$tutorial_path = $GLOBALS['base_url'] . '/' . $mod_path . '/tutorial/_tutorial(GoogleMapNode).pdf';
	$form = array();
	$form['intro'] = array(
		'#type' => 'markup',
		'#markup' => '<object data="' . $tutorial_path . '" type="application/pdf" width="100%" height="100%" >
 
  <p>It seems like you don\'t have a PDF plugin for this browser.
  No problem sir... you can <a href="' . $tutorial_path . '">click here to
  download the PDF file.</a></p>
  
</object>',
	'#prefix' => '<div style = "width:100%; height :100%">',
	'#suffix' => '</div>',
		);
	return $form;
}