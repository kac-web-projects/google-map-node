<?php
/*
Form provind content-type to choose
*/
function gmap_nodes_choose_types($form, &$form_state) {
	
	$form = array();

	$form['intro'] = array(
		'#type' => 'markup',
		'#markup' => 'Please select the content-types which should have feature of gmap',
		);

	$obj = node_type_get_types();
	// dpm(print_r($obj,true));
	$options = array();
	
	foreach ($obj as $single) {
	
		$form[$single->type] = array(
			'#type' => 'fieldset',
			'#title' => t($single->type),
			'#collapsible' => TRUE,
			'#collapsed' => TRUE, 
			);

		$form[$single->type][$single->type . '-enable'] = array(
			'#type' => 'checkbox',
			'#title' => t('Enable'),
			'#name' => $single->type . '-enable',
			'#default_value' => FALSE,
			'#return_value' => '1',
			);

		$form[$single->type][$single->type] = array(
				'#type' => 'fieldset',
				'#title' => 'Fields',
				'#description' => 'Select which values to use as latitude and longitude',
				'#collapsible' => TRUE,
				'#collapsed' => FALSE,
				);

		$form[$single->type][$single->type][$single->type . '-target'] = array(
			'#type' => 'textfield',
			'#default_value' => 'map-canvas',
			'#title' => t('Target Element - ID'),
			// '#required' => TRUE,
			);

		$form[$single->type][$single->type]['#states'] = array(
				'visible' => array(
					':input[name=' . $single->type. '-enable]' => array('checked' => TRUE),
					),
				// 'required' => array(
					// ':input[name=' . $single->type. '-enable]' => array('checked' => TRUE),
					// ),
				);
		
		// $form[$single->type][$single->type . '-target']['#states'] = array(
		// 		'visible' => array(
		// 			':input[name=' . $single->type. '-enable]' => array('checked' => TRUE),
		// 			),
		// 		);


		$fields = field_info_instances("node",$single->type);
		$fields = array_keys($fields);
		$options = array();
		foreach ($fields as $key => $value) {
			$options[$value] = $value;
		}
		$form[$single->type][$single->type][$single->type . '-latitude'] = array(
				'#type' => 'select',
				'#options' => $options,
				'#title' => 'Latitude',
				// '#required' => TRUE,
		);
		
		// $form[$single->type][$single->type]['latitude']['#states'] = array(
		// 		'required' => array(
		// 			':input[name=' . $single->type. '-enable]' => array('checked' => TRUE),
		// 			),
		// );
		
		
		$form[$single->type][$single->type][$single->type . '-longitude'] = array(
				'#type' => 'select',
				'#options' => $options,
				'#title' => 'Longitude',
				// '#required' => TRUE,
		);
		
		// $form[$single->type][$single->type]['longitude']['#states'] = array(
		// 		'required' => array(
		// 			':input[name=' . $single->type. '-enable]' => array('checked' => TRUE),
		// 			),
		// );
	

		$form[$single->type][$single->type][$single->type . '-markers'] = array(
				'#type' => 'select',
				'#options' => $options,
				'#title' => 'Markers',
				// '#required' => TRUE,
		);
	
		// $form[$single->type][$single->type][$single->type . '-markers'] = array(
		// 		'#type' => 'textarea',
		// 		'#title' => 'Markers',
		// 		// '#required' => TRUE,
		// );
		
		/* Previous Value if applicable */
		$attached_nodes = variable_get('gmap_node_nodes','NULL');
	
		if($attached_nodes != 'NULL') {
			// dpm("not null");
			$sel_types = array();
			$sel_target = array();
			$sel_lat = array();
			$sel_long = array();
			$sel_markers = array();
	
		foreach ($attached_nodes as $node) {
			$node = explode("#",$node);
			array_push($sel_types, $node[0]);
			array_push($sel_lat, $node[1]);
			array_push($sel_long, $node[2]);
			array_push($sel_markers, $node[3]);
			array_push($sel_target, $node[4]);
		}
	
		if(in_array($single->type, $sel_types)) {
			$key = array_search($single->type,$sel_types);
			$form[$single->type][$single->type . '-enable']['#default_value'] = TRUE;
			$form[$single->type][$single->type][$single->type . '-target']['#default_value'] = $sel_target[$key];
			$form[$single->type][$single->type][$single->type . '-latitude']['#default_value'] = $sel_lat[$key];
			$form[$single->type][$single->type][$single->type . '-longitude']['#default_value'] = $sel_long[$key];
			$form[$single->type][$single->type][$single->type . '-markers']['#default_value'] = $sel_markers[$key];
		}

		}

	}

	$form['submit'] = array(
		'#type' => 'submit',
		'#value' => 'Save Changes',
		);
	
	$form['reset'] = array(
		'#type' => 'markup',
		'#markup' => '<input type=reset  class=form-submit />'
		);
	
	return $form;
}

/* Validating it */

function gmap_nodes_choose_types_validate($form,&$form_state) {
	// dpm($form_state['values']);

	$obj = node_type_get_types();
	foreach ($obj as $single) {
		$type = $form_state['values'][$single->type . '-enable'];
		if($type) {
			$target = $form_state['values'][$single->type . '-target'];
			$latitude = $form_state['values'][$single->type . '-latitude'];
			$longitude = $form_state['values'][$single->type . '-longitude'];
			$markers = $form_state['values'][$single->type . '-markers'];

			if(empty($target))
			form_set_error($single->type . '-target','Target element can\'t be empty - Target Element - ID (' . $single->type . ')' );
	
			$field_instance = field_info_instance('node', $latitude, $single->type);
			if($field_instance['display']['default']['type'] != 'number_decimal')
			form_set_error($single->type . '-latitude','Must be a Float field-type for allowing decimal values - Latitude (' . $single->type . ')' );

			$field_instance = field_info_instance('node', $longitude, $single->type);
			if($field_instance['display']['default']['type'] != 'number_decimal')
			form_set_error($single->type . '-longitude','Must be a Float field-type for allowing decimal values - Longitude (' . $single->type . ')');
			
			$field_instance = field_info_instance('node', $markers, $single->type);
			$allowed = array("text_textarea","text_textarea_with_summary");
			if(!in_array($field_instance['widget']['type'],$allowed))
			form_set_error($single->type . '-markers','Must be a textarea or body field - Markers ( ' . $single->type . ')');

			// dpm($field_instance);
		}
	}

}

/* Saving the required value into VARIABLE table */
function gmap_nodes_choose_types_submit($form, &$form_state) {
	$attach_node = array();
	$obj = node_type_get_types();
	foreach ($obj as $single) {
		$type = $form_state['values'][$single->type . '-enable'];
		if($type) {
			$latitude = $form_state['values'][$single->type . '-latitude'];
			$longitude = $form_state['values'][$single->type . '-longitude'];
			$markers = $form_state['values'][$single->type . '-markers'];
			$eid = $form_state['values'][$single->type . '-target'];
		array_push($attach_node, $single->type . '#' . $latitude . '#' . $longitude . '#' . $markers . '#' . $eid);
		}
	}
	variable_set('gmap_node_nodes',$attach_node);
	drupal_flush_all_caches();
	drupal_set_message("Settings Saved. Clear Cache in case changes are not in effect");
}