/* Adding Javascript Behavior for Nodes */
Drupal.behaviors.gmap_node = {
  attach : function()
  {
    google.maps.event.addDomListener(window, 'load',nodeMap(Drupal.settings.gmap_node.latitude,Drupal.settings.gmap_node.longtitude,Drupal.settings.gmap_node.markers,Drupal.settings.gmap_node.eid,Drupal.settings.gmap_node.zoom,Drupal.settings.gmap_node.mtype,Drupal.settings.gmap_node.nanimation,Drupal.settings.gmap_node.mdraggable,Drupal.settings.gmap_node.n_icon_type,Drupal.settings.gmap_node.modpath,Drupal.settings.gmap_node.n_fname));
  }
}

/* For Nodes */
function nodeMap(latitude,longtitude,markers,eid,zoom,mtype,animation,draggable,icon_type,modpath,n_fname) {
  
  var mapOptions = {
    'zoom' : parseInt(zoom),
    center: new google.maps.LatLng(latitude,longtitude),
    'mapTypeId': eval(mtype),
	};
	
  var map = new google.maps.Map(document.getElementById(eid),mapOptions);
 
  var tmarkers = markers.split('\n');
  var len = tmarkers.length;
  for(i = 0 ; i < len  ; i++) {
    var vals = tmarkers[i].split(',');
    var latlang = new google.maps.LatLng(vals[0],vals[1]);
    // alert(latlang);

    var title = vals[2];

  var choice;
  switch(icon_type) {
    case 'image':
      choice = '../' + modpath + '/micons/sports/' + n_fname;
      break;
    case 'star':
      choice = {
        path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
        fillColor: 'violet',
        fillOpacity: 0.8,
        scale: 0.1,
        strokeColor: 'red',
        strokeWeight: 1,
      };
      break;
    case 'circle':
      choice = {
        path: google.maps.SymbolPath.CIRCLE,
        fillColor: 'red',
        fillOpacity: 0.8,
        scale: 10,
        strokeColor: 'violet',
        strokeWeight: 2,
      };
      break;
  }
    var marker = new google.maps.Marker({
      position: latlang,
      map: map,
      icon : choice,
      title: title,
      animation: eval(animation),
      draggable : eval(draggable),
    });
  
  // google.maps.event.addListener(marker, 'click', toggleBounce);
  }
}
/* For toogling Bounce */
// function toggleBounce() {

//   if (marker.getAnimation() != null) {
//     marker.setAnimation(null);
//   } else {
//     marker.setAnimation(google.maps.Animation.BOUNCE);
//   }
// }